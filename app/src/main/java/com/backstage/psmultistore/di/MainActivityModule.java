package com.backstage.psmultistore.di;


import com.backstage.psmultistore.MainActivity;
import com.backstage.psmultistore.ui.apploading.AppLoadingActivity;
import com.backstage.psmultistore.ui.apploading.AppLoadingFragment;
import com.backstage.psmultistore.ui.basket.BasketListActivity;
import com.backstage.psmultistore.ui.basket.BasketListFragment;
import com.backstage.psmultistore.ui.blog.detail.BlogDetailActivity;
import com.backstage.psmultistore.ui.blog.detail.BlogDetailFragment;
import com.backstage.psmultistore.ui.blog.list.BlogListActivity;
import com.backstage.psmultistore.ui.blog.list.BlogListFragment;
import com.backstage.psmultistore.ui.blog.listbyshopid.BlogListByShopIdActivity;
import com.backstage.psmultistore.ui.blog.listbyshopid.BlogListByShopIdFragment;
import com.backstage.psmultistore.ui.category.CategoryListActivity;
import com.backstage.psmultistore.ui.category.CategoryListFragment;
import com.backstage.psmultistore.ui.category.TrendingCategoryFragment;
import com.backstage.psmultistore.ui.checkout.CheckoutActivity;
import com.backstage.psmultistore.ui.checkout.CheckoutFragment1;
import com.backstage.psmultistore.ui.checkout.CheckoutFragment2;
import com.backstage.psmultistore.ui.checkout.CheckoutFragment3;
import com.backstage.psmultistore.ui.checkout.CheckoutStatusFragment;
import com.backstage.psmultistore.ui.privacyandpolicy.PrivacyAndPolicyActivity;
import com.backstage.psmultistore.ui.privacyandpolicy.PrivacyAndPolicyFragment;
import com.backstage.psmultistore.ui.product.search.SearchCityListFragment;
import com.backstage.psmultistore.ui.product.search.SearchCountryListFragment;
import com.backstage.psmultistore.ui.stripe.StripeActivity;
import com.backstage.psmultistore.ui.collection.CollectionActivity;
import com.backstage.psmultistore.ui.collection.CollectionFragment;
import com.backstage.psmultistore.ui.collection.productCollectionHeader.ProductCollectionHeaderListActivity;
import com.backstage.psmultistore.ui.collection.productCollectionHeader.ProductCollectionHeaderListFragment;
import com.backstage.psmultistore.ui.comment.detail.CommentDetailActivity;
import com.backstage.psmultistore.ui.comment.detail.CommentDetailFragment;
import com.backstage.psmultistore.ui.comment.list.CommentListActivity;
import com.backstage.psmultistore.ui.comment.list.CommentListFragment;
import com.backstage.psmultistore.ui.contactus.ContactUsFragment;
import com.backstage.psmultistore.ui.dashboard.DashBoardShopListFragment;
import com.backstage.psmultistore.ui.forceupdate.ForceUpdateActivity;
import com.backstage.psmultistore.ui.forceupdate.ForceUpdateFragment;
import com.backstage.psmultistore.ui.gallery.GalleryActivity;
import com.backstage.psmultistore.ui.gallery.GalleryFragment;
import com.backstage.psmultistore.ui.gallery.detail.GalleryDetailActivity;
import com.backstage.psmultistore.ui.gallery.detail.GalleryDetailFragment;
import com.backstage.psmultistore.ui.language.LanguageFragment;
import com.backstage.psmultistore.ui.notification.detail.NotificationActivity;
import com.backstage.psmultistore.ui.notification.detail.NotificationFragment;
import com.backstage.psmultistore.ui.notification.list.NotificationListActivity;
import com.backstage.psmultistore.ui.notification.list.NotificationListFragment;
import com.backstage.psmultistore.ui.notification.setting.NotificationSettingFragment;
import com.backstage.psmultistore.ui.product.detail.ProductActivity;
import com.backstage.psmultistore.ui.product.detail.ProductDetailFragment;
import com.backstage.psmultistore.ui.product.favourite.FavouriteListActivity;
import com.backstage.psmultistore.ui.product.favourite.FavouriteListFragment;
import com.backstage.psmultistore.ui.product.filtering.CategoryFilterFragment;
import com.backstage.psmultistore.ui.product.filtering.FilterFragment;
import com.backstage.psmultistore.ui.product.filtering.FilteringActivity;
import com.backstage.psmultistore.ui.product.history.HistoryFragment;
import com.backstage.psmultistore.ui.product.history.UserHistoryListActivity;
import com.backstage.psmultistore.ui.product.list.ProductListActivity;
import com.backstage.psmultistore.ui.product.list.ProductListFragment;
import com.backstage.psmultistore.ui.product.productbycatId.ProductListByCatIdActivity;
import com.backstage.psmultistore.ui.product.productbycatId.ProductListByCatIdFragment;
import com.backstage.psmultistore.ui.product.search.SearchByCategoryActivity;
import com.backstage.psmultistore.ui.product.search.SearchCategoryFragment;
import com.backstage.psmultistore.ui.product.search.SearchFragment;
import com.backstage.psmultistore.ui.product.search.SearchSubCategoryFragment;
import com.backstage.psmultistore.ui.rating.RatingListActivity;
import com.backstage.psmultistore.ui.rating.RatingListFragment;
import com.backstage.psmultistore.ui.setting.AppInfoActivity;
import com.backstage.psmultistore.ui.setting.AppInfoFragment;
import com.backstage.psmultistore.ui.setting.TermsAndConditionsActivity;
import com.backstage.psmultistore.ui.setting.NotificationSettingActivity;
import com.backstage.psmultistore.ui.setting.SettingActivity;
import com.backstage.psmultistore.ui.setting.SettingFragment;
import com.backstage.psmultistore.ui.shop.detail.ShopFragment;
import com.backstage.psmultistore.ui.shop.list.ShopListActivity;
import com.backstage.psmultistore.ui.shop.list.ShopListFragment;
import com.backstage.psmultistore.ui.shop.listbytagid.ShopListByTagIdActivity;
import com.backstage.psmultistore.ui.shop.listbytagid.ShopListByTagIdFragment;
import com.backstage.psmultistore.ui.shop.menu.ShopMenuFragment;
import com.backstage.psmultistore.ui.shop.selectedshop.SelectedShopActivity;
import com.backstage.psmultistore.ui.shop.selectedshop.SelectedShopFragment;
import com.backstage.psmultistore.ui.shop.tag.ShopTagListActivity;
import com.backstage.psmultistore.ui.shop.tag.ShopTagListFragment;
import com.backstage.psmultistore.ui.stripe.StripeFragment;
import com.backstage.psmultistore.ui.terms.TermsAndConditionsFragment;
import com.backstage.psmultistore.ui.transaction.detail.TransactionActivity;
import com.backstage.psmultistore.ui.transaction.detail.TransactionFragment;
import com.backstage.psmultistore.ui.transaction.list.TransactionListActivity;
import com.backstage.psmultistore.ui.transaction.list.TransactionListFragment;
import com.backstage.psmultistore.ui.user.PasswordChangeActivity;
import com.backstage.psmultistore.ui.user.PasswordChangeFragment;
import com.backstage.psmultistore.ui.user.ProfileEditActivity;
import com.backstage.psmultistore.ui.user.ProfileEditFragment;
import com.backstage.psmultistore.ui.user.ProfileFragment;
import com.backstage.psmultistore.ui.user.UserForgotPasswordActivity;
import com.backstage.psmultistore.ui.user.UserForgotPasswordFragment;
import com.backstage.psmultistore.ui.user.UserLoginActivity;
import com.backstage.psmultistore.ui.user.UserLoginFragment;
import com.backstage.psmultistore.ui.user.UserRegisterActivity;
import com.backstage.psmultistore.ui.user.UserRegisterFragment;
import com.backstage.psmultistore.ui.user.phonelogin.PhoneLoginActivity;
import com.backstage.psmultistore.ui.user.phonelogin.PhoneLoginFragment;
import com.backstage.psmultistore.ui.user.verifyemail.VerifyEmailActivity;
import com.backstage.psmultistore.ui.user.verifyemail.VerifyEmailFragment;
import com.backstage.psmultistore.ui.user.verifyphone.VerifyMobileActivity;
import com.backstage.psmultistore.ui.user.verifyphone.VerifyMobileFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Panacea-Soft on 11/15/17.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = TransactionModule.class)
    abstract TransactionListActivity contributeTransactionActivity();

    @ContributesAndroidInjector(modules = FavouriteListModule.class)
    abstract FavouriteListActivity contributeFavouriteListActivity();

    @ContributesAndroidInjector(modules = UserHistoryModule.class)
    abstract UserHistoryListActivity contributeUserHistoryListActivity();

    @ContributesAndroidInjector(modules = UserRegisterModule.class)
    abstract UserRegisterActivity contributeUserRegisterActivity();

    @ContributesAndroidInjector(modules = UserForgotPasswordModule.class)
    abstract UserForgotPasswordActivity contributeUserForgotPasswordActivity();

    @ContributesAndroidInjector(modules = UserLoginModule.class)
    abstract UserLoginActivity contributeUserLoginActivity();

    @ContributesAndroidInjector(modules = PasswordChangeModule.class)
    abstract PasswordChangeActivity contributePasswordChangeActivity();

    @ContributesAndroidInjector(modules = ProductListByCatIdModule.class)
    abstract ProductListByCatIdActivity productListByCatIdActivity();

    @ContributesAndroidInjector(modules = FilteringModule.class)
    abstract FilteringActivity filteringActivity();

    @ContributesAndroidInjector(modules = CommentDetailModule.class)
    abstract CommentDetailActivity commentDetailActivity();

    @ContributesAndroidInjector(modules = DiscountDetailModule.class)
    abstract ProductActivity discountDetailActivity();

    @ContributesAndroidInjector(modules = NotificationModule.class)
    abstract NotificationListActivity notificationActivity();

    @ContributesAndroidInjector(modules = HomeFilteringActivityModule.class)
    abstract ProductListActivity contributehomeFilteringActivity();

    @ContributesAndroidInjector(modules = NotificationDetailModule.class)
    abstract NotificationActivity notificationDetailActivity();

    @ContributesAndroidInjector(modules = TransactionDetailModule.class)
    abstract TransactionActivity transactionDetailActivity();

    @ContributesAndroidInjector(modules = CheckoutActivityModule.class)
    abstract CheckoutActivity checkoutActivity();

    @ContributesAndroidInjector(modules = CommentListActivityModule.class)
    abstract CommentListActivity commentListActivity();

    @ContributesAndroidInjector(modules = BasketlistActivityModule.class)
    abstract BasketListActivity basketListActivity();

    @ContributesAndroidInjector(modules = GalleryDetailActivityModule.class)
    abstract GalleryDetailActivity galleryDetailActivity();

    @ContributesAndroidInjector(modules = GalleryActivityModule.class)
    abstract GalleryActivity galleryActivity();

    @ContributesAndroidInjector(modules = SearchByCategoryActivityModule.class)
    abstract SearchByCategoryActivity searchByCategoryActivity();

    @ContributesAndroidInjector(modules = TermsAndConditionsModule.class)
    abstract com.backstage.psmultistore.ui.terms.TermsAndConditionsActivity termsAndConditionsActivity();

    @ContributesAndroidInjector(modules = EditSettingModule.class)
    abstract SettingActivity editSettingActivity();

    @ContributesAndroidInjector(modules = LanguageChangeModule.class)
    abstract NotificationSettingActivity languageChangeActivity();

    @ContributesAndroidInjector(modules = ProfileEditModule.class)
    abstract ProfileEditActivity contributeProfileEditActivity();

    @ContributesAndroidInjector(modules = TermsAndConditionsModule.class)
    abstract TermsAndConditionsActivity ConditionsAndTermsActivity();

    @ContributesAndroidInjector(modules = AppInfoModule.class)
    abstract AppInfoActivity AppInfoActivity();

    @ContributesAndroidInjector(modules = ProductCollectionModule.class)
    abstract ProductCollectionHeaderListActivity productCollectionHeaderListActivity();

    @ContributesAndroidInjector(modules = CategoryListActivityAppInfoModule.class)
    abstract CategoryListActivity categoryListActivity();

    @ContributesAndroidInjector(modules = RatingListActivityModule.class)
    abstract RatingListActivity ratingListActivity();

    @ContributesAndroidInjector(modules = ShopListModule.class)
    abstract SelectedShopActivity selectedShopActivity();

    @ContributesAndroidInjector(modules = CollectionModule.class)
    abstract CollectionActivity collectionActivity();

    @ContributesAndroidInjector(modules = StripeModule.class)
    abstract StripeActivity stripeActivity();

    @ContributesAndroidInjector(modules = SelectedShopListModule.class)
    abstract ShopListActivity selectedShopListActivity();

    @ContributesAndroidInjector(modules = SelectedShopListBlogModule.class)
    abstract BlogListActivity selectedShopListBlogActivity();

    @ContributesAndroidInjector(modules = BlogDetailModule.class)
    abstract BlogDetailActivity blogDetailActivity();

    @ContributesAndroidInjector(modules = ShopCategoryDetailModule.class)
    abstract ShopListByTagIdActivity shopCategoryDetailActivity();

    @ContributesAndroidInjector(modules = ShopCategoryViewAllModule.class)
    abstract ShopTagListActivity shopCategoryViewAllActivity();

    @ContributesAndroidInjector(modules = forceUpdateModule.class)
    abstract ForceUpdateActivity forceUpdateActivity();

    @ContributesAndroidInjector(modules = blogListByShopIdActivityModule.class)
    abstract BlogListByShopIdActivity forceBlogListByShopIdActivity();

    @ContributesAndroidInjector(modules = appLoadingActivityModule.class)
    abstract AppLoadingActivity forceAppLoadingActivity();

    @ContributesAndroidInjector(modules = PrivacyAndPolicyActivityModule.class)
    abstract PrivacyAndPolicyActivity privacyAndPolicyActivity();

    @ContributesAndroidInjector(modules = VerifyEmailModule.class)
    abstract VerifyEmailActivity contributeVerifyEmailActivity();

    @ContributesAndroidInjector(modules = PhoneLoginActivityModule.class)
    abstract PhoneLoginActivity contributePhoneLoginActivity();

    @ContributesAndroidInjector(modules = VerifyMobileModule.class)
    abstract VerifyMobileActivity contributeVerifyMobileActivity();
}

@Module
abstract class CheckoutActivityModule {

    @ContributesAndroidInjector
    abstract CheckoutFragment1 checkoutFragment1();

    @ContributesAndroidInjector
    abstract LanguageFragment languageFragment();

    @ContributesAndroidInjector
    abstract CheckoutFragment2 checkoutFragment2();

    @ContributesAndroidInjector
    abstract CheckoutFragment3 checkoutFragment3();

    @ContributesAndroidInjector
    abstract CheckoutStatusFragment checkoutStatusFragment();
}

@Module
abstract class MainModule {

    @ContributesAndroidInjector
    abstract ContactUsFragment contributeContactUsFragment();

    @ContributesAndroidInjector
    abstract UserLoginFragment contributeUserLoginFragment();

    @ContributesAndroidInjector
    abstract UserForgotPasswordFragment contributeUserForgotPasswordFragment();

    @ContributesAndroidInjector
    abstract UserRegisterFragment contributeUserRegisterFragment();

    @ContributesAndroidInjector
    abstract NotificationSettingFragment contributeNotificationSettingFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment contributeProfileFragment();

    @ContributesAndroidInjector
    abstract LanguageFragment contributeLanguageFragment();

    @ContributesAndroidInjector
    abstract FavouriteListFragment contributeFavouriteListFragment();

    @ContributesAndroidInjector
    abstract TransactionListFragment contributTransactionListFragment();

    @ContributesAndroidInjector
    abstract SettingFragment contributEditSettingFragment();

    @ContributesAndroidInjector
    abstract HistoryFragment historyFragment();

    @ContributesAndroidInjector
    abstract PrivacyAndPolicyFragment privacyAndPolicyFragment();

    @ContributesAndroidInjector
    abstract NotificationListFragment contributeNotificationFragment();


    @ContributesAndroidInjector
    abstract AppInfoFragment contributeAppInfoFragment();

    @ContributesAndroidInjector
    abstract DashBoardShopListFragment contributeShopListFragment();

    @ContributesAndroidInjector
    abstract VerifyEmailFragment contributeVerifyEmailFragment();

    @ContributesAndroidInjector
    abstract PhoneLoginFragment contributePhoneLoginFragment();

    @ContributesAndroidInjector
    abstract VerifyMobileFragment contributeVerifyMobileFragment();
}


@Module
abstract class ProfileEditModule {
    @ContributesAndroidInjector
    abstract ProfileEditFragment contributeProfileEditFragment();
}

@Module
abstract class TransactionModule {
    @ContributesAndroidInjector
    abstract TransactionListFragment contributeTransactionListFragment();
}

@Module
abstract class FavouriteListModule {
    @ContributesAndroidInjector
    abstract FavouriteListFragment contributeFavouriteFragment();
}

@Module
abstract class UserRegisterModule {
    @ContributesAndroidInjector
    abstract UserRegisterFragment contributeUserRegisterFragment();
}

@Module
abstract class UserForgotPasswordModule {
    @ContributesAndroidInjector
    abstract UserForgotPasswordFragment contributeUserForgotPasswordFragment();
}

@Module
abstract class UserLoginModule {
    @ContributesAndroidInjector
    abstract UserLoginFragment contributeUserLoginFragment();
}

@Module
abstract class PasswordChangeModule {
    @ContributesAndroidInjector
    abstract PasswordChangeFragment contributePasswordChangeFragment();
}

@Module
abstract class CommentDetailModule {
    @ContributesAndroidInjector
    abstract CommentDetailFragment commentDetailFragment();
}

@Module
abstract class DiscountDetailModule {
    @ContributesAndroidInjector
    abstract ProductDetailFragment discountDetailFragment();
}

@Module
abstract class NotificationModule {
    @ContributesAndroidInjector
    abstract NotificationListFragment notificationFragment();


}


@Module
abstract class NotificationDetailModule {
    @ContributesAndroidInjector
    abstract NotificationFragment notificationDetailFragment();
}

@Module
abstract class TransactionDetailModule {
    @ContributesAndroidInjector
    abstract TransactionFragment transactionDetailFragment();
}

@Module
abstract class UserHistoryModule {
    @ContributesAndroidInjector
    abstract HistoryFragment contributeHistoryFragment();
}

@Module
abstract class AppInfoModule {
    @ContributesAndroidInjector
    abstract AppInfoFragment contributeAppInfoFragment();
}

@Module
abstract class ProductCollectionModule {
    @ContributesAndroidInjector
    abstract ProductCollectionHeaderListFragment contributeProductCollectionHeaderListFragment();
}

@Module
abstract class CategoryListActivityAppInfoModule {
    @ContributesAndroidInjector
    abstract CategoryListFragment contributeCategoryFragment();

    @ContributesAndroidInjector
    abstract TrendingCategoryFragment contributeTrendingCategoryFragment();
}

@Module
abstract class RatingListActivityModule {
    @ContributesAndroidInjector
    abstract RatingListFragment contributeRatingListFragment();
}

@Module
abstract class TermsAndConditionsModule {
    @ContributesAndroidInjector
    abstract TermsAndConditionsFragment TermsAndConditionsFragment();
}

@Module
abstract class EditSettingModule {
    @ContributesAndroidInjector
    abstract SettingFragment EditSettingFragment();
}

@Module
abstract class LanguageChangeModule {
    @ContributesAndroidInjector
    abstract NotificationSettingFragment notificationSettingFragment();
}

@Module
abstract class EditProfileModule {
    @ContributesAndroidInjector
    abstract ProfileFragment ProfileFragment();
}

@Module
abstract class ProductListByCatIdModule {
    @ContributesAndroidInjector
    abstract ProductListByCatIdFragment contributeProductListByCatIdFragment();

}

@Module
abstract class FilteringModule {

    @ContributesAndroidInjector
    abstract CategoryFilterFragment contributeTypeFilterFragment();

    @ContributesAndroidInjector
    abstract FilterFragment contributeSpecialFilteringFragment();
}

@Module
abstract class HomeFilteringActivityModule {
    @ContributesAndroidInjector
    abstract ProductListFragment contributefeaturedProductFragment();

    @ContributesAndroidInjector
    abstract CategoryListFragment contributeCategoryFragment();

    @ContributesAndroidInjector
    abstract CategoryFilterFragment contributeTypeFilterFragment();

    @ContributesAndroidInjector
    abstract CollectionFragment contributeCollectionFragment();
}

@Module
abstract class CommentListActivityModule {
    @ContributesAndroidInjector
    abstract CommentListFragment contributeCommentListFragment();
}

@Module
abstract class BasketlistActivityModule {
    @ContributesAndroidInjector
    abstract BasketListFragment contributeBasketListFragment();
}

@Module
abstract class GalleryDetailActivityModule {
    @ContributesAndroidInjector
    abstract GalleryDetailFragment contributeGalleryDetailFragment();
}

@Module
abstract class GalleryActivityModule {
    @ContributesAndroidInjector
    abstract GalleryFragment contributeGalleryFragment();
}

@Module
abstract class SearchByCategoryActivityModule {

    @ContributesAndroidInjector
    abstract SearchCategoryFragment contributeSearchCategoryFragment();

    @ContributesAndroidInjector
    abstract SearchSubCategoryFragment contributeSearchSubCategoryFragment();

    @ContributesAndroidInjector
    abstract SearchCountryListFragment contributeSearchCountryListFragment();

    @ContributesAndroidInjector
    abstract SearchCityListFragment contributeSearchCityListFragment();

}

@Module
abstract class ShopListModule {

    @ContributesAndroidInjector
    abstract ShopFragment contributeAboutUsFragmentFragment();

    @ContributesAndroidInjector
    abstract BasketListFragment basketFragment();

    @ContributesAndroidInjector
    abstract SearchFragment contributeSearchFragment();

    @ContributesAndroidInjector
    abstract SelectedShopFragment contributeHomeFragment();

    @ContributesAndroidInjector
    abstract CategoryFilterFragment contributeTypeFilterFragment();

    @ContributesAndroidInjector
    abstract ProductCollectionHeaderListFragment contributeProductCollectionHeaderListFragment();

    @ContributesAndroidInjector
    abstract ShopMenuFragment contributeShopMenuFragment();


}

@Module
abstract class CollectionModule {

    @ContributesAndroidInjector
    abstract CollectionFragment contributeCollectionFragment();

}

@Module
abstract class StripeModule {

    @ContributesAndroidInjector
    abstract StripeFragment contributeStripeFragment();

}

@Module
abstract class SelectedShopListModule {

    @ContributesAndroidInjector
    abstract ShopListFragment contributeSelectedShopListFragment();

}

@Module
abstract class SelectedShopListBlogModule {

    @ContributesAndroidInjector
    abstract BlogListFragment contributeSelectedShopListBlogFragment();

}

@Module
abstract class BlogDetailModule {

    @ContributesAndroidInjector
    abstract BlogDetailFragment contributeBlogDetailFragment();
}

@Module
abstract class ShopCategoryDetailModule {

    @ContributesAndroidInjector
    abstract ShopListByTagIdFragment contributeShopCategoryDetailFragment();
}

@Module
abstract class ShopCategoryViewAllModule {

    @ContributesAndroidInjector
    abstract ShopTagListFragment contributeShopCategoryViewAllFragment();
}


@Module
abstract class forceUpdateModule {

    @ContributesAndroidInjector
    abstract ForceUpdateFragment contributeForceUpdateFragment();
}

@Module
abstract class blogListByShopIdActivityModule {

    @ContributesAndroidInjector
    abstract BlogListByShopIdFragment contributeBlogListByShopIdFragment();
}

@Module
abstract class appLoadingActivityModule {

    @ContributesAndroidInjector
    abstract AppLoadingFragment contributeAppLoadingFragment();
}
@Module
abstract class PrivacyAndPolicyActivityModule {

    @ContributesAndroidInjector
    abstract PrivacyAndPolicyFragment contributePrivacyAndPolicyFragment();

}
@Module
abstract class VerifyEmailModule {
    @ContributesAndroidInjector
    abstract VerifyEmailFragment contributeVerifyEmailFragment();
}

@Module
abstract class PhoneLoginActivityModule {
    @ContributesAndroidInjector
    abstract PhoneLoginFragment cameraPhoneLoginFragment();
}

@Module
abstract class VerifyMobileModule {
    @ContributesAndroidInjector
    abstract VerifyMobileFragment contributeVerifyMobileFragment();
}
