package com.backstage.psmultistore.di;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.backstage.psmultistore.Config;
import com.backstage.psmultistore.api.PSApiService;
import com.backstage.psmultistore.db.AboutUsDao;
import com.backstage.psmultistore.db.BasketDao;
import com.backstage.psmultistore.db.BlogDao;
import com.backstage.psmultistore.db.CategoryDao;
import com.backstage.psmultistore.db.CategoryMapDao;
import com.backstage.psmultistore.db.CityDao;
import com.backstage.psmultistore.db.CommentDao;
import com.backstage.psmultistore.db.CommentDetailDao;
import com.backstage.psmultistore.db.CountryDao;
import com.backstage.psmultistore.db.DeletedObjectDao;
import com.backstage.psmultistore.db.HistoryDao;
import com.backstage.psmultistore.db.ImageDao;
import com.backstage.psmultistore.db.NotificationDao;
import com.backstage.psmultistore.db.PSAppInfoDao;
import com.backstage.psmultistore.db.PSAppVersionDao;
import com.backstage.psmultistore.db.PSCoreDb;
import com.backstage.psmultistore.db.ProductAttributeDetailDao;
import com.backstage.psmultistore.db.ProductAttributeHeaderDao;
import com.backstage.psmultistore.db.ProductCollectionDao;
import com.backstage.psmultistore.db.ProductColorDao;
import com.backstage.psmultistore.db.ProductDao;
import com.backstage.psmultistore.db.ProductMapDao;
import com.backstage.psmultistore.db.ProductSpecsDao;
import com.backstage.psmultistore.db.RatingDao;
import com.backstage.psmultistore.db.ShippingMethodDao;
import com.backstage.psmultistore.db.ShopDao;
import com.backstage.psmultistore.db.ShopListByTagIdDao;
import com.backstage.psmultistore.db.ShopTagDao;
import com.backstage.psmultistore.db.SubCategoryDao;
import com.backstage.psmultistore.db.TransactionDao;
import com.backstage.psmultistore.db.TransactionOrderDao;
import com.backstage.psmultistore.db.UserDao;
import com.backstage.psmultistore.utils.AppLanguage;
import com.backstage.psmultistore.utils.Connectivity;
import com.backstage.psmultistore.utils.LiveDataCallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import androidx.room.Room;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Panacea-Soft on 11/15/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Module(includes = ViewModelModule.class)
class AppModule {

    @Singleton
    @Provides
    PSApiService providePSApiService() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

        return new Retrofit.Builder()
                .baseUrl(Config.APP_API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(PSApiService.class);

    }

    @Singleton
    @Provides
    PSCoreDb provideDb(Application app) {
        return Room.databaseBuilder(app, PSCoreDb.class, "PSApp.db")
                //.addMigrations(MIGRATION_1_2)
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    Connectivity provideConnectivity(Application app) {
        return new Connectivity(app);
    }

    @Singleton
    @Provides
    SharedPreferences provideSharedPreferences(Application app) {
        return PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
    }

    @Singleton
    @Provides
    UserDao provideUserDao(PSCoreDb db) {
        return db.userDao();
    }

    @Singleton
    @Provides
    AppLanguage provideCurrentLanguage(SharedPreferences sharedPreferences) {
        return new AppLanguage(sharedPreferences);
    }

    @Singleton
    @Provides
    AboutUsDao provideAboutUsDao(PSCoreDb db) {
        return db.aboutUsDao();
    }

    @Singleton
    @Provides
    ImageDao provideImageDao(PSCoreDb db) {
        return db.imageDao();
    }

    @Singleton
    @Provides
    CountryDao provideCountryDao(PSCoreDb db) {
        return db.countryDao();
    }

    @Singleton
    @Provides
    CityDao provideCityDao(PSCoreDb db) {
        return db.cityDao();
    }

    @Singleton
    @Provides
    ProductDao provideProductDao(PSCoreDb db) {
        return db.productDao();
    }

    @Singleton
    @Provides
    ProductColorDao provideProductColorDao(PSCoreDb db) {
        return db.productColorDao();
    }

    @Singleton
    @Provides
    ProductSpecsDao provideProductSpecsDao(PSCoreDb db) {
        return db.productSpecsDao();
    }

    @Singleton
    @Provides
    ProductAttributeHeaderDao provideProductAttributeHeaderDao(PSCoreDb db) {
        return db.productAttributeHeaderDao();
    }

    @Singleton
    @Provides
    ProductAttributeDetailDao provideProductAttributeDetailDao(PSCoreDb db) {
        return db.productAttributeDetailDao();
    }

    @Singleton
    @Provides
    BasketDao provideBasketDao(PSCoreDb db) {
        return db.basketDao();
    }

    @Singleton
    @Provides
    HistoryDao provideHistoryDao(PSCoreDb db) {
        return db.historyDao();
    }

    @Singleton
    @Provides
    CategoryDao provideCategoryDao(PSCoreDb db) {
        return db.categoryDao();
    }

    @Singleton
    @Provides
    RatingDao provideRatingDao(PSCoreDb db) {
        return db.ratingDao();
    }

    @Singleton
    @Provides
    SubCategoryDao provideSubCategoryDao(PSCoreDb db) {
        return db.subCategoryDao();
    }

    @Singleton
    @Provides
    CommentDao provideCommentDao(PSCoreDb db) {
        return db.commentDao();
    }

    @Singleton
    @Provides
    CommentDetailDao provideCommentDetailDao(PSCoreDb db) {
        return db.commentDetailDao();
    }

    @Singleton
    @Provides
    NotificationDao provideNotificationDao(PSCoreDb db) {
        return db.notificationDao();
    }

    @Singleton
    @Provides
    ProductCollectionDao provideProductCollectionDao(PSCoreDb db) {
        return db.productCollectionDao();
    }

    @Singleton
    @Provides
    TransactionDao provideTransactionDao(PSCoreDb db) {
        return db.transactionDao();
    }

    @Singleton
    @Provides
    TransactionOrderDao provideTransactionOrderDao(PSCoreDb db) {
        return db.transactionOrderDao();
    }

//    @Singleton
//    @Provides
//    TrendingCategoryDao provideTrendingCategoryDao(PSCoreDb db){return db.trendingCategoryDao();}

    @Singleton
    @Provides
    ShopDao provideShopDao(PSCoreDb db) {
        return db.shopDao();
    }

    @Singleton
    @Provides
    ShopTagDao provideShopCategoryDao(PSCoreDb db) {
        return db.shopCategoryDao();
    }

    @Singleton
    @Provides
    BlogDao provideNewsFeedDao(PSCoreDb db) {
        return db.blogDao();
    }

    @Singleton
    @Provides
    ShippingMethodDao provideShippingMethodDao(PSCoreDb db) {
        return db.shippingMethodDao();
    }

    @Singleton
    @Provides
    ShopListByTagIdDao provideShopCategoryByIdDao(PSCoreDb db) {
        return db.shopListByTagIdDao();
    }

    @Singleton
    @Provides
    ProductMapDao provideProductMapDao(PSCoreDb db) {
        return db.productMapDao();
    }

    @Singleton
    @Provides
    CategoryMapDao provideCategoryMapDao(PSCoreDb db) {
        return db.categoryMapDao();
    }

    @Singleton
    @Provides
    PSAppInfoDao providePSAppInfoDao(PSCoreDb db) {
        return db.psAppInfoDao();
    }

    @Singleton
    @Provides
    PSAppVersionDao providePSAppVersionDao(PSCoreDb db) {
        return db.psAppVersionDao();
    }

    @Singleton
    @Provides
    DeletedObjectDao provideDeletedObjectDao(PSCoreDb db) {
        return db.deletedObjectDao();
    }
}
