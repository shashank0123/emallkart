package com.backstage.psmultistore.viewmodel.paypal;

import com.backstage.psmultistore.repository.paypal.PaypalRepository;
import com.backstage.psmultistore.utils.AbsentLiveData;
import com.backstage.psmultistore.utils.Utils;
import com.backstage.psmultistore.viewmodel.common.PSViewModel;
import com.backstage.psmultistore.viewobject.common.Resource;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

public class PaypalViewModel extends PSViewModel {

    private final LiveData<Resource<Boolean>> paypalTokenData;
    private MutableLiveData<String> paypalTokenObj = new MutableLiveData<>();


    @Inject
    PaypalViewModel(PaypalRepository repository) {
        paypalTokenData = Transformations.switchMap(paypalTokenObj, obj -> {
            if (obj == null) {
                return AbsentLiveData.create();
            }
            Utils.psLog("paypalTokenData");
            return repository.getPaypalToekn(obj);
        });
    }

    public void setPaypalTokenObj(String shopId) {
        this.paypalTokenObj.setValue(shopId);
    }

    public LiveData<Resource<Boolean>> getPaypalTokenData() {
        return paypalTokenData;
    }

}
